-- This platformer is using the Baseline 2D Platform page on the Love2D wiki as a starting point.

platform = {}
player = {}

function love.load()
    -- This is the height and width of the platform.
    platform.width = love.graphics.getWidth()       -- This makes the platform as wide as the game window.
    platform.height = love.graphics.getHeight()     -- This makes the platform as high as the game window. (No shit!)

    -- This is the coordinates where the platform will be rendered.
    platform.x = 0                      -- This starts drawing the platform at the very left of the screen.
    platform.y = platform.height / 2    -- This starts drawing the platform at the middle of the screen.

    -- This is the coordinates where the player will be rendered. (I just saw my dog move his paws while sleeping lol)
    player.x = love.graphics.getWidth() / 2     -- This sets the player at the middle of the screen.
    player.y = love.graphics.getHeight() / 2    -- Same thing as the last statement.

    -- This calls the image below and puts it in a variable called player.img
    player.img = love.graphics.newImage('purple.png')

    player.ground = player.y        -- This makes the player land on the platform.
    player.y_velocity = 0           -- When the player hasn't jumped yet, Y-Axis is always 0.
    player.jump_height = -300       -- When player jumps, they reach this height.
    player.gravity = -500           -- When player falls, they descend this quickly.

    player.speed = 200  -- 200 fucking what? mph? kph? WE NEED ANSWERS!!! /s
end

function love.update(dt)
    if love.keyboard.isDown('d') then
        -- This makes sure the player doesn't go past the window to the right.
        if player.x < (love.graphics.getWidth() - player.img:getWidth()) then
            player.x = player.x + (player.speed * dt)
        end
    elseif love.keyboard.isDown('a') then
        -- Same thing but with the left.
        if player.x > 0 then
            player.x = player.x - (player.speed * dt)
        end
    end
    
    -- This is in charge of the jumping key.
    if love.keyboard.isDown('space') then
        -- Game checks if player is on the ground.
        if player.y_velocity == 0 then
            player.y_velocity = player.jump_height
        end
    end

    -- This is in charge of the jumping physics.
    if player.y_velocity ~= 0 then                                      -- The game checks if player has "jumped" and left the ground.
        player.y = player.y + player.y_velocity * dt                    -- This makes the player jump.
        player.y_velocity = player.y_velocity - player.gravity * dt     -- This applies the gravity to the player.
    end

    -- This is in charge of collision, making sure the player lands on the ground and not sink into the void.
    if player.y > player.ground then        -- Checks if the player has jumped.
        player.y_velocity = 0               -- The Y-Axis Velocity is set back to 0 meaning the player is on the ground again
        player.y = player.ground            -- Same shit as the last comment.
    end
end

function love.draw()
    love.graphics.setColor(1, 0, 1)     -- This sets the platform colour to what I fucking want. (magenta)

    -- The platform will now be drawn.
    love.graphics.rectangle('fill', platform.x, platform.y, platform.width, platform.height)

    -- Drawing the player
    love.graphics.draw(player.img, player.x, player.y, 0, 1, 1, 0, 32) -- Purple ontop of magenta is fucking stupid now that I think about it.
end